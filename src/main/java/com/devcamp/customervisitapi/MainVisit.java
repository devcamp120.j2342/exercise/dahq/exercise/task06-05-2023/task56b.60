package com.devcamp.customervisitapi;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainVisit {
    @CrossOrigin
    @GetMapping("/visits")
    public ArrayList<Visit> visitsList() {
        Customer customer1 = new Customer("Nguyen Van A", false, "Premium");
        Customer customer2 = new Customer("Nguyen Van B", true, "Premium");
        Customer customer3 = new Customer("Nguyen Van C", false, "Basic");
        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        System.out.println(customer3.toString());

        Visit visit1 = new Visit(customer3, new Date(), 1200, 10);
        Visit visit2 = new Visit(customer2, new Date(), 1200, 10);
        Visit visit3 = new Visit(customer1, new Date(), 1200, 10);
        System.out.println(visit1.toString());
        System.out.println(visit2.toString());
        System.out.println(visit3.toString());
        ArrayList<Visit> visits = new ArrayList<>();
        visits.add(visit3);
        visits.add(visit2);
        visits.add(visit1);
        return visits;

    }

}
